# encoding:utf-8
"""
（本周共计3个一个扩展作业）
1. 在计算机上安装OpenCV 3.0以上版本，为后续作业准备。开发语言可采用C++或Python。
2. 创建第一个视觉程序“Hello，world！”，显示Lena图片。具体效果参看课程PPT。
3. 对Lena图像，分解得到RGB分量及HSV分量，显示各分量，并对结果进行比较说明。

"""

import cv2

# Materials covered by Dr. Qu
# 第2题40分，在窗口中正确输出Lena彩色图像得分。
img = cv2.imread(r'D:\OpenCV4.2.0\sources\samples\data\lena.jpg')
cv2.imshow('Hello, world!', img) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/Lena.JPG
cv2.waitKey()
cv2.destroyAllWindows()

# 第3题60分，在窗口中正确显示RGB分量得20分，正确显示HSV分量得20分，结果分析比较说明合理得20分。
# 分析比较: 将图像从RGB色彩空间转换到了HSV色彩空间，可更好地感知图像颜色的变化，因为HSV模型更加符合人类感知颜色的方式：
#          颜色、深浅以及亮暗，因此利用HSV分量更容易从图像中提取感兴趣的区域。比如我们要提取美女的头发区域，就可以通过
#          设置HSV色彩空间的高低阈值来实现。
#          RGB模式虽然容易理解及显示，但在图像处理上并没有特别的优势，因为只是将图像进行三原色的分析，却没有特别利用到
#          人眼的视觉特性。
#          以Lena为例，头发在HSV模式中特别明显，很容易利用阈值加以提取或删除，但在RGB模式下就很难达成这个目标。
# Extract R、G、B components from Lena
(B,G,R) = cv2.split(img)
cv2.imshow("Red",R) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/R.JPG
cv2.imshow("Green",G) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/G.JPG
cv2.imshow("Blue",B) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/B.JPG
cv2.waitKey(0)
cv2.destroyAllWindows()

# Transform RGB to HSV space
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
cv2.imshow("Hue", hsv[:, :, 0]) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/H.JPG
cv2.imshow("Saturation", hsv[:, :, 1]) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/S.JPG
cv2.imshow("Value", hsv[:, :, 2]) # https://gitee.com/skyrookieyu/csdn_ai25_week1/blob/master/V.JPG
cv2.waitKey(0)
cv2.destroyAllWindows()
